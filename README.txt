HOW TO SETUP THE GAME

This game uses a node.js server in order to establish a secure connection to a MongoDB database.
In order for you to play on your machine, you need to install Node.js first.
Then under cmd navigate to the game backend folder and run the following commands:
npm install
npm run dev
After this, you will have your Node.js server runing and you can open the main game html.

##################################

HOW TO PLAY

1. The computer will select a word ("_ _ _ _ _") for you to guess.

2. Begin by guessing one letter of the alphabet.
For example, guess "A".
You can either click on the letter or type into the keyboard.

3. If your guess is in the word, it will be revealed in its correct position(s). If not, a part of the hangman is drawn by the computer.

4. Continue guessing one letter at a time, trying to complete the word while avoiding incorrect guesses.

5. Win by guessing the word before the computer completes drawing the hangman.