const dbName = "HangmanGame";
let db;

export async function getDb(client) {
  db = await client.db(dbName);
}

export async function getWords() {
  try {
    const result = await db.collection("word_list").find().toArray();

    if (result.length > 0) return result;
    else return console.log("No matching documents found.");
  }
  catch (err) {
    return { error: err.name + ", " + err.message };
  }
}

export async function getAllUsers() {
  try {
    return await db.collection("user_config").find().toArray();
  }
  catch (err) {
    return { error: err.name + ", " + err.message };
  }
}

export async function validateUser(name, password) {
  try {
    const query = {name};

    const result = await db.collection("user_config").findOne(query);

    // console.log(result);

    if (result === null) {
      const createUser = await upsertUser(name, {win: 0, loss: 0}, password);

      const { error } = createUser;
      
      if (error) return { error_code: 500, error_msg: error };
      else return await db.collection("user_config").findOne(query);
    }
    else {
      // console.log(result.password, password);

      if (result.password != undefined) {
        if (result.password == password) return result;
        else return { error_code: 401, error_msg: "Password not match!" };
      }
      else {
        const updateUserPwd = await upsertUser(name, null, password);

        const { error } = updateUserPwd;
        
        if (error) return { error_code: 500, error_msg: error };
        else return await db.collection("user_config").findOne(query);
      }
    }
  }
  catch (err) {
    return { error_code: 500, error_msg: err.name + ", " + err.message };
  }
}

export async function upsertUser(name, stats = null, password = null) {
  try {
    const query = {name};
    const update = {$set: {}};
    const option = {upsert: true};

    if (stats !== null) update.$set.stats = stats;
    if (password !== null) update.$set.password = password;

    // console.log(update);

    return await db.collection("user_config").updateOne(query, update, option);
  }
  catch (err) {
    return { error: err.name + ", " + err.message };
  }
}

export async function getUserByName(name) {
  try {
    const filter = {name};

    return await db.collection("user_config").findOne(filter);
  }
  catch (err) {
    return { error: err.name + ", " + err.message };
  }
}