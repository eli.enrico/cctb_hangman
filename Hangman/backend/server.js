import express from "express";
import cors from "cors";
import database from "./database.js";
import { getDb, getWords, getAllUsers, validateUser, upsertUser, getUserByName } from "./controllers.js";

const PORT = 8000;
const app = express(); // create middleware

app.use(cors());
app.use(express.static("Hangman")); // to load the files in the "Hangman" directory
app.use(express.json()); // to parse incoming requests with JSON payloads

// route definition
app.get("/api/words", async (req, res) => {
  try {
    const docs = await getWords();

    res.json(docs);
  }
  catch (err) {
    console.error("# GET words error", err);

    res.status(500).send({ error: err.name + ", " + err.message });
  }
});

app.get("/api/users", async (req, res) => {
  try {
    const docs = await getAllUsers();

    res.json(docs);
  }
  catch (err) {
    console.error("# GET users error", err);

    res.status(500).send({ error: err.name + ", " + err.message });
  }
});

app.get("/api/users/validate", async (req, res) => {
  try {
    const name = req.query.name.toUpperCase();
    const password = req.query.password;

    let result = await validateUser(name, password);

    // console.log(result);

    if (result.error_code !== undefined) res.status(result.error_code).send(result);
    else res.json(result);
  }
  catch (err) {
    console.error("# GET validate user error: ", err);

    res.status(500).send({ error: err });
  }
});

app.put("/api/users/update", async (req, res) => {
  try {
    const name = req.body.name;
    const stats = req.body.stats;

    const updateResponse = await upsertUser(name, stats);

    let { error } = updateResponse;

    if (error) res.status(500).json({ error });
    else res.status(200).json({ status: "success" });

    // if (updateResponse.modifiedCount === 0) {
    //   throw new Error("unable to update");
    // }

    // res.json({ status: "success" });
  }
  catch (err) {
    console.error("# PUT user stats error", err);

    res.status(500).send({ error: err.name + ", " + err.message });
  }
});

app.get("/api/users/:name", async (req, res) => {
  try {
    const name = req.params.name;
    const docs = await getUserByName(name);

    res.json(docs);
  }
  catch (err) {
    console.error("# GET user by name error", err);

    res.status(500).send({ error: err.name + ", " + err.message });
  }
});

// Start the web server and connect to the database
let server;
let conn;

// Immediately-Invoked Function Expression (IIFE): The entire arrow function is wrapped in parentheses and immediately invoked ( ... )().
// This pattern is used to create a function and execute it immediately.
(async () => {
  try {
    conn = await database();

    await getDb(conn);

    server = app.listen(PORT, () => console.log(`listening on port: ${PORT}`));
  }
  catch (err) {
    console.error("# Error:", err);
    console.error("# Exiting the application.");

    await closing();

    process.exit(1);
  }
})();

async function closing() {
  console.log("# Closing resources...");

  if (conn) {
    await conn.close();

    console.log("# Database connection closed.");
  }

  if (server) server.close(() => console.log("# Web server stopped."));
}