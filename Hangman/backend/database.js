import { MongoClient } from "mongodb";
export const mongoURI =
  "mongodb+srv://ryosuke:ZRqE9eYdxXNFMgRM@cluster0.s1qxues.mongodb.net/HangmanGame?retryWrites=true&w=majority";

const mongoClient = new MongoClient(mongoURI);

export default async () => {
  try {
    console.log("# Connecting to database server...");
    const client = await mongoClient.connect();
    console.log("# Connected");
    return client;
  } catch (e) {
    console.error("# Database connection error");
    console.error(e);
  }
};
