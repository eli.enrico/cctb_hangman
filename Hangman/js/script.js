$(function () {
  const GC_gameNotStarted  = 0;
  const GC_playingGame  = 1;
  const GC_gameFinished = 2;

  const allowedKeys = "ABCDEFGHIJKLMNOPQRSTUVXYWZ";

  let gameStatus = GC_gameNotStarted;
  let words = "";
  let currentWord = "";
  let guessedWord = "";
  let incorrectGuesses = 0;
  let currentPlayerName = "";
  let currentStats = {
    win: 0,
    loss: 0
  }

  const getWordsURL = "http://localhost:8000/api/words/";
  const validateUserURL = "http://localhost:8000/api/users/validate/";
  const updateUserURL = "http://localhost:8000/api/users/update/";

  function readWordsFile() {
    fetch(getWordsURL)
      .then((response) => response.json())
      .then((data) => words = data)
      .catch((error) => console.error("Error fetching the words: ", error));
  }

  function setButtons() {
    for (let i in allowedKeys) {
      const char = allowedKeys[i];
      const button = document.createElement("button");

      button.textContent = char;
      button.id = "btn-" + char;
      button.classList.add("btn");

      button.onclick = function () {
        guessChar(char);
      };

      $("#words-button-container").append(button);
    }
  }

  function startGame() {
    if (currentPlayerName === "") return;

    const level = $("#no-hint-toggle").is(":checked") ? "H" : "E";

    incorrectGuesses = 0;

    const randomIndex = Math.floor(Math.random() * words.length);
    const selectedWord = words[randomIndex].word.toUpperCase();
    const selectedHint = words[randomIndex].hint.toUpperCase();

    console.log(selectedWord);
    currentWord = selectedWord;

    $("#game-container").css({ display: "block" });

    const infoMessage =
      level.toUpperCase() == "E"
        ? "Hint: " + selectedHint
        : 'No hint<br /><span class="notes">(to change it, go to <span class="bold">SETTINGS</span>)</span>';

    $(".message-container")
      .removeClass()
      .addClass("message-container")
      .addClass("info-message");
    $(".message-content").html(infoMessage);
    $("#words-button-container > button")
      .removeClass()
      .addClass("btn")
      .removeAttr("disabled");
    $("#hangman-image").attr("src", "images/hangman_0.png");
    $("#retry-game-button").css({ display: "none" });

    guessedWord = Array(selectedWord.length).fill("_").join("");

    $("#word-label").text(guessedWord);

    $("#welcome-page").animate({ top: "100px", height: window.innerHeight - 100, opacity: 0 }, 500, function () {
      $(this).css({ display: "none" });

      gameStatus = GC_playingGame;
    });
  }

  function updateUI() {
    $("#word-label").text(guessedWord);
    $("#hangman-image").attr("src", "images/hangman_" + incorrectGuesses + ".png");

    if (guessedWord === currentWord || incorrectGuesses >= 6) {
      let addClass = "";
      let msgContent = "";
      let image = "";

      if (guessedWord === currentWord) {
        addClass = "success-message";
        msgContent = "You win!";
        image = "images/win.png";

        currentStats.win++;
      }
      else {
        addClass = "danger-message";
        msgContent = "You lose!";
        image = "images/lose.png";

        currentStats.loss++;
      }

      $(".message-container")
        .removeClass("info-message")
        .addClass(addClass);
      $(".message-content").html(msgContent);

      if (incorrectGuesses >= 6) $("#word-label").text(currentWord);

      $("#hangman-image").attr("src", image);
      $("#retry-game-button").css({ display: "block" });

      gameStatus = GC_gameFinished;

      updateScores();
    }
  }

  function guessChar(char) {
    if (gameStatus !== GC_playingGame) return;

    let addStyle = "incorrect";

    $("#btn-" + char).attr("disabled", "true");

    if (currentWord.includes(char)) {
      for (let i = 0; i < currentWord.length; i++) {
        if (currentWord[i] == char) {
          guessedWord =
            guessedWord.substring(0, i) + char + guessedWord.substring(i + 1);
        }
      }

      addStyle = "correct";
    }
    else incorrectGuesses++;

    $("#btn-" + char).addClass(addStyle);

    updateUI();
  }

  function retryGame() {
    startGame();
  }

  function quitGame() {
    $("#welcome-page")
      .css({ display: "flex" })
      .animate({ top: 0, height: "100vh", opacity: 1 }, 500, function () {
        resetUser();
      });
  }

  function resetUser() {
    currentPlayerName = "";
    currentStats = {
      win: 0,
      loss: 0
    }

    $("#user").val("");
    $("#password").val("");
  }

  function updateScores() {
    console.log(currentPlayerName + ' :: ' + currentStats.win + " :: " + currentStats.loss);
  
    fetch(updateUserURL, {
      method: "PUT",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ name: currentPlayerName, stats: currentStats })
    })
      .then((response) => response.json())
      .then(() => displayStatistics())
      .catch((error) => console.error("Error updating scores on server: ", error));
  }

  function displayStatistics() {
    const totalWins = currentStats.win;
    const totalLosses = currentStats.loss;
    const totalGames = totalWins + totalLosses;
    const winPercentage = totalGames === 0 ? 0 : Math.round((totalWins / totalGames) * 100);
    const lossPercentage = totalGames === 0 ? 0 : Math.round((totalLosses / totalGames) * 100);

    const htmlContent = `
      <h3>User: ${currentPlayerName}</h3>
      <div class="values-container">
        <p><span>${totalGames}</span><br />Played</p>
        <p><span>${totalWins}</span><br />Win</p>
        <p><span>${totalLosses}</span><br />Loss</p>
      </div>
      <div class="graph-container">
        <div class="graph-item">
          <p>Win %</p>
          <div><span class="wins" data-value="${winPercentage}">${winPercentage}&nbsp;%&nbsp;</span></div>
        </div>
        <div class="graph-item">
          <p>Loss %</p>
          <div><span class="losses" data-value="${lossPercentage}">${lossPercentage}&nbsp;%&nbsp;</span></div>
        </div>
      </div>
    `;

    $("#statistics-page .statistics-container").html(htmlContent);
  }

  function animateStatistics() {
    $(".graph-container .graph-item > div > span").each(function () {
      const dataValue = $(this).data("value");

      $(this).animate({ width: `${dataValue}%`, opacity: 1 }, 1500);
    });
  }

  function showSplashPage(splashPage) {
    $(splashPage)
      .css({ display: "flex" })
      .animate({ top: 0, height: "100vh", opacity: 1 }, 500, function () {
        $(this).css({ "background-color": "rgba(255, 255, 255, 0.5)" });

        if (splashPage === "#statistics-page") animateStatistics();
      });
  }

  function closeSplashPage(splashPage) {
    $(splashPage)
      .css({ "background-color": "transparent" })
      .animate({ top: "100px", height: window.innerHeight - 100, opacity: 0 }, 500, function () {
        $(this).css({ display: "none" });

        switch(splashPage) {
          case "#statistics-page":
            $(".statistics-container .graph-container .graph-item span").css({
              width: 0,
              opacity: 0,
            });
            break;

          case "#login-page":
            startGame();
            break;
        }
      });
  }

  function showLoginForm() {
    showSplashPage('#login-page');
  }

  $(document).on("keypress", function (event) {
    if ($("#no-keypress-toggle").is(":checked")) return;

    const keyPressed = event.key.toUpperCase();

    if (allowedKeys.includes(keyPressed)) guessChar(keyPressed);
  });

  $("#start-game-button").on("click", function () {
    showLoginForm();
  });

  $("#retry-game-button").on("click", function () {
    retryGame();
  });

  $("#quit-game-button").on("click", function () {
    quitGame();
  });

  $("header .button-container .btn, #how-to-button").on("click", function () {
    const splashPage = $(this).data("splashPage");

    if (splashPage === "logout") quitGame();
    else showSplashPage(`#${splashPage}-page`);
  });

  $(".splash-page .btn-close").on("click", function () {
    closeSplashPage(`#${$(this).parents(".splash-page").attr("id")}`);
  });

  $("#login-form").submit(function (e) {
    e.preventDefault();

    const user = $("#user").val();
    const password = $("#password").val();

    if (user === "") alert("Enter your user!");
    else if (password === "") alert("Enter your password!");
    else {
      fetch(`${validateUserURL}?name=${user}&password=${password}`, {
        method: "GET",
        headers: { "Content-Type": "application/json" }
      })
        .then((response) => {
          // console.log(response);

          return response.json();
        })
        .then((data) => {
          console.log(data);

          if (data.error_code === undefined) {
            currentPlayerName = data.name.toUpperCase();
            currentStats.win = data.stats.win;
            currentStats.loss = data.stats.loss;

            displayStatistics();

            closeSplashPage(`#${$(this).parents(".splash-page").attr("id")}`);
          }
          else alert(`${data.error_msg} (${data.error_code})`);
        })
        .catch((error) => alert(error));
    }
  });

  readWordsFile();
  setButtons();
});
